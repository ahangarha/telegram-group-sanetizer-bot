#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Mostafa Ahangarha
# Licence: GPLv3
# Description: This is a telegram bot made to handle posts in groups
# with sensitive content.

from telegram.ext import Updater, Filters
from telegram.ext import CommandHandler, MessageHandler
import logging
import os


# Reading variables from .env file
def getEnvVar(varName):
    from os.path import join, dirname
    from dotenv import load_dotenv
    dotenv_path = join(dirname(__file__), '.env')

    # TODO: Check if file exists of not.

    load_dotenv(dotenv_path)

    result = os.environ.get(varName)
    return(result)


def start(bot, update):
    text = "Welcome to Group Sanetizer Bot."

    bot.send_message(
        chat_id=update.message.chat_id,
        text=text,
        parse_mode='Markdown',
    )

def isSensitive(content):
    # Here we should detect if the content includes sensitive words
    # The current condition is very simple and only for demonstration
    # Returning False means it doesn't have sensitive content
    if (content == "sensitive"):
        return(True)
    return(False)

def msgHandle(bot, update):
    chat_id = update.message.chat_id
    message_id = update.message.message_id
    logging.info("Chat_id={}, Message_id={}".format(
        chat_id,
        message_id
        )
    )
    logging.info("Content: \"{}\"".format(update.message.text))
    if (isSensitive(update.message.text )):
        logging.info("sensitive content detected!!!")
        try:
            bot.deleteMessage(chat_id, message_id)
            logging.info("Message deleted!")
        except:
            logging.info("Failed to delete the message")

# ///////////////////////////////////////////////

logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s',
    level=logging.INFO,
)

updater = Updater(getEnvVar('TOKEN'))
dispatcher = updater.dispatcher

dispatcher.add_handler(CommandHandler('start', start))

# Analyse text posts
dispatcher.add_handler(MessageHandler(Filters.text, msgHandle))


updater.start_polling()
print("The bot is running...")
updater.idle()
